/* 
 * File:   GIOM3000.cpp
 * Author: krupkaf
 * 
 * Created on 5. října 2018, 10:33
 */

#include "GIOM3000.h"
#include <stdlib.h>

GIOM3000::GIOM3000(uint16_t port) {
    begin(port);
}

GIOM3000::GIOM3000() {
}

void GIOM3000::begin(uint16_t port) {
    Udp.begin(port);
}

int8_t GIOM3000::parser(uint8_t *buffer, uint8_t packetLen) {
    uint8_t i, tmp, uitmp;
    uint8_t *p;
    float ftmp;

    if (packetLen != 145) {
        //Chybna delka paketu
        return -1;
    }

    p = buffer;
    tmp = 0x00;
    for (i = 0; i < 145; i++) {
        tmp ^= *p;
        p++;
    }
    if (tmp != 0x00) {
        //Nesouhlasi "Checksum"
        return -2;
    }

    p = buffer;
    for (i = 0; i < 18; i++) { //Ve zprave je 18 polozek po 8 bytech
        ftmp = 0;
        uitmp = 0;
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                if ((p[0] != ' ') || (p[1] != ' ')) {
                    //Poloznka nema ocekavany tvar.
                    return -3;
                }
                tmp = p[8]; //Schovam si znak na pozici 8(prvni znak naledujici polozky)
                p[8] = 0x00; //A vlozim sem konec retezce.
                ftmp = atof((const char*) p);
                p[8] = tmp; //Vratim to do puvodniho stavu.
                break;
            case 6:
                if ((p[0] != ' ') || (p[1] != ' ') || (p[2] != ' ') || (p[3] != ' ') || (p[4] != ' ')) {
                    //Poloznka nema ocekavany tvar.
                    return -4;
                }
                tmp = p[8]; //Schovam si znak na pozici 8(prvni znak naledujici polozky)
                p[8] = 0x00; //A vlozim sem konec retezce.
                uitmp = atoi((const char*) p);
                p[8] = tmp; //Vratim to do puvodniho stavu.
                break;
            case 17:
                if ((p[0] != 'G') || (p[1] != 'I') || (p[2] != 'O') || (p[3] != 'M') || (p[4] != ' ')) {
                    //Poloznka nema ocekavany tvar. Chybi oznaceni zarizeni
                    return -5;
                }
                break;
        }
        switch (i) {
            case 0:
                barometricALT = ftmp;
                break;
            case 1:
                absolutePress = ftmp;
                break;
            case 2:
                relativePress = ftmp;
                break;
            case 3:
                windSPEED = ftmp;
                break;
            case 4:
                windGUST = ftmp;
                break;
            case 5:
                windAVG = ftmp;
                break;
            case 6:
                windDir = uitmp;
                break;
            case 8:
                windDirDeg = ftmp;
                break;
            case 10:
                steamPress = ftmp;
                break;
            case 11:
                relativeHumidity = ftmp;
                break;
            case 12:
                dewPoint = ftmp;
                break;
            case 13:
                temperature = ftmp;
                break;
            case 14:
                windchill = ftmp;
                break;
            case 15:
                absHumidity_g_m3 = ftmp;
                break;
            case 16:
                absHumidity_g_kg = ftmp;
                break;
        }
        p += 8;
    }
    return 1; //Hotovo.
}

int8_t GIOM3000::update(void) {
    int packetSize = Udp.parsePacket();
    if (packetSize > 0) {
        uint8_t packetBuffer[146];
        Udp.read(packetBuffer, 146);
        return parser(packetBuffer, packetSize);
    } else {
        //Neni zadny paket ke zpracovani, aktualizace neprovedena.
        return 0;
    }
}

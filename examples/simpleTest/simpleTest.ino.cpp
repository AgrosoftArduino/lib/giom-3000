/**
 * Jednoduchy test knihovny GIOM3000
 * 
 * HW: Arduino s ethernet modulem (W5100), meteostanice GIOM 3000
 * 
 * Meteostanice se nastavi, aby na Arduino posilala UDP pakety s daty na port 3000.
 * Arduino pakety zpracuje a data zobrazi na seriovem terminalu.
 * 
 */


#include <Arduino.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <GIOM3000.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 20, 35);

GIOM3000 meteo;

void setup() {
    Ethernet.begin(mac, ip);
    Serial.begin(9600);
    meteo.begin(3000);
}

void loop() {
    if (meteo.update() == 1) {
        Serial.print(F("State            : "));
        Serial.println();
        Serial.print(F("Barometric ALT   : "));
        Serial.println(meteo.barometricALT, 1);
        Serial.print(F("Absolute Press   : "));
        Serial.println(meteo.absolutePress, 1);
        Serial.print(F("Relative Press   : "));
        Serial.println(meteo.relativePress, 1);
        Serial.print(F("Wind SPEED       : "));
        Serial.println(meteo.windSPEED, 1);
        Serial.print(F("Wind GUST        : "));
        Serial.println(meteo.windGUST, 1);
        Serial.print(F("Wind SPEED       : "));
        Serial.println(meteo.windSPEED, 1);
        Serial.print(F("Wind AVG         : "));
        Serial.println(meteo.windAVG, 1);
        Serial.print(F("Wind dir         : "));
        Serial.println(meteo.windDir, 1);
        Serial.print(F("Wind dir deg     : "));
        Serial.println(meteo.windDirDeg, 1);
        Serial.print(F("Steam Press      : "));
        Serial.println(meteo.steamPress, 1);
        Serial.print(F("Relative humidity: "));
        Serial.println(meteo.relativeHumidity, 1);
        Serial.print(F("Dew Point        : "));
        Serial.println(meteo.dewPoint, 1);
        Serial.print(F("Temperature      : "));
        Serial.println(meteo.temperature, 1);
        Serial.print(F("Windchill        : "));
        Serial.println(meteo.windchill, 1);
        Serial.print(F("Abs. hum. g/m3   : "));
        Serial.println(meteo.absHumidity_g_m3, 1);
        Serial.print(F("Abs. hum. g/kg   : "));
        Serial.println(meteo.absHumidity_g_kg, 1);
        Serial.println(F("-------------------"));
    }
}

/* 
 * File:   GIOM3000.h
 * Author: krupkaf
 *
 * Created on 5. října 2018, 10:33
 */

#ifndef GIOM3000_H
#define GIOM3000_H

#include <stdint.h>
#include <EthernetUdp.h>

class GIOM3000 {
public:
    float barometricALT;
    float absolutePress;
    float relativePress;
    float windSPEED;
    float windGUST;
    float windAVG;
    uint8_t windDir;
    float windDirDeg;
    float steamPress;
    float relativeHumidity;
    float dewPoint;
    float temperature;
    float windchill;
    float absHumidity_g_m3;
    float absHumidity_g_kg;

    /**
     * port - port na kterem se nasloucha.
     */
    GIOM3000(uint16_t port);
    GIOM3000();
    void begin(uint16_t port);

    /**
     * vrati: 1 - pokud vse ok, aktualizace dat probehla.
     *        0 - nebyl prijat paket s aktualizaci, aktualizace neprovedena.
     *  zaporne - Byl prijat paket, ale neobsahuje pouzitelna data. V tomto pripade mohou byt nektere polozky zmeneny.
     */
    int8_t update(void);
private:
    int8_t parser(uint8_t *buffer, uint8_t packetLen);
    EthernetUDP Udp;
};

#endif /* GIOM3000_H */
